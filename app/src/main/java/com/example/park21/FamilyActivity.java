package com.example.park21;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;

import com.example.park21.models.Familia;
import com.example.park21.viewmodels.FamiliaActivityViewModel;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

public class FamilyActivity extends AppCompatActivity {

    private RecyclerView mRecyclerView;
    private FamilyRecyclerViewAdapter mAdapter;
    private FamiliaActivityViewModel familiaActivityViewModel;
    private ProgressBar mProgressBar;
    private FloatingActionButton agregar;

    private Familia mFamilia;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_family);

        mRecyclerView = findViewById(R.id.recycler_view_family);
        mProgressBar = findViewById(R.id.progress_bar_family);
        familiaActivityViewModel = ViewModelProviders.of(this).get(FamiliaActivityViewModel.class);
        agregar = findViewById(R.id.AgregarMiembro);
        agregar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showAddItemDialog(FamilyActivity.this);
            }
        });

        familiaActivityViewModel.init();

        familiaActivityViewModel.getIsUpdating().observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(@Nullable Boolean aBoolean) {
                if(aBoolean){
                    showProgressBar();
                }
                else{
                    hideProgressBar();
                    mRecyclerView.smoothScrollToPosition(familiaActivityViewModel.getNicePlaces().getValue().getUsuarios().size()-1);
                }
            }
        });

        initRecyclerView();
    }

    private void showProgressBar(){
        mProgressBar.setVisibility(View.VISIBLE);
    }

    private void hideProgressBar(){
        mProgressBar.setVisibility(View.GONE);
    }

    private void initRecyclerView(){

        mAdapter = new FamilyRecyclerViewAdapter(this, familiaActivityViewModel.getNicePlaces().getValue().getUsuarios());
        RecyclerView.LayoutManager linearLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(linearLayoutManager);
        mRecyclerView.setAdapter(mAdapter);
    }

    private void showAddItemDialog(Context c) {
        final EditText taskEditText = new EditText(c);
        AlertDialog dialog = new AlertDialog.Builder(c)
                .setTitle("Agrega un nuevo Miembro")
                .setMessage("Agrega su cédula")
                .setView(taskEditText)
                .setPositiveButton("Agregar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //TODO Lógica de buscar un miembro ya existente si no mostrar error
                        String task = String.valueOf(taskEditText.getText());
                    }
                })
                .setNegativeButton("Cancelar", null)
                .create();
        dialog.show();
    }
}
