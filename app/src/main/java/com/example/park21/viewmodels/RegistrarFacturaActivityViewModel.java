package com.example.park21.viewmodels;

import androidx.lifecycle.MutableLiveData;

import com.example.park21.models.Usuario;
import com.example.park21.repositories.Repository;

public class RegistrarFacturaActivityViewModel {
    private MutableLiveData<Usuario> usuario;
    private Repository mRepo;

    public void init(){
        if(usuario != null){
            return;
        }
        mRepo = Repository.getInstance();
        usuario = mRepo.getUsuario();
    }

    public void setFactura(){
        mRepo.setFacturas();
    }


}
