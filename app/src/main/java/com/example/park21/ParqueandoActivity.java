package com.example.park21;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.lifecycle.ViewModelProviders;

import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.example.park21.viewmodels.PagarParqActivityViewModel;
import com.example.park21.viewmodels.ParqueandoActivityViewModel;

public class ParqueandoActivity extends AppCompatActivity {

    public static final String EXTRA_MESSAGE = "com.example.myfirstapp.MESSAGE";
    private CardView pagar, verCarro, salir;
    private ParqueandoActivityViewModel viewModel;
    private String parqEscogido;
    private String tiempo;
    private TextView textView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_parqueando);
        viewModel = ViewModelProviders.of(this).get(ParqueandoActivityViewModel.class);

        viewModel.init();
        Intent intent = getIntent();

        parqEscogido = viewModel.getUbicacion();
        tiempo = viewModel.getTiempo();
        if (intent != null){
            Bundle extras = intent.getExtras();
            if (extras != null){
                parqEscogido = viewModel.getUbicacion();
                tiempo = viewModel.getTiempo();
            }
        }
        if(savedInstanceState != null){
            parqEscogido = savedInstanceState.getString("EXTRA_NAME");
            tiempo = savedInstanceState.getString("EXTRA_TIME");
        }


        textView = findViewById(R.id.textViewParqueaderoActual);
        textView.setText(parqEscogido);

        pagar = (CardView) findViewById(R.id.pagarParq);
        pagar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent myIntent = new Intent(ParqueandoActivity.this,
                        PagarParqActivity.class);
                Bundle extras = new Bundle();
                extras.putString("EXTRA_NAME",parqEscogido);
                extras.putString("EXTRA_TIME",tiempo);
                myIntent.putExtras(extras);
                startActivity(myIntent);
            }
        });

        verCarro = (CardView) findViewById(R.id.verCarro);
        verCarro.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                if(isNetworkReachable()){
                    Intent myIntent = new Intent(ParqueandoActivity.this,
                            SnapshotAutomovilActivity.class);
                    startActivity(myIntent);
                }
                else Toast.makeText(ParqueandoActivity.this, "Debes estar conectado a internet para realizar esta operación.",
                        Toast.LENGTH_SHORT).show();
            }
        });
        salir = (CardView) findViewById(R.id.salirParq);
        salir.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                if (viewModel.getPagar()){
                    viewModel.salir();
                    Intent myIntent = new Intent(ParqueandoActivity.this,
                            SalirParqActivity.class);
                    startActivity(myIntent);
                }
                else Toast.makeText(ParqueandoActivity.this, "Debes pagar antes de salir",
                        Toast.LENGTH_SHORT).show();

            }
        });


    }

    // This callback is called only when there is a saved instance that is previously saved by using
// onSaveInstanceState(). We restore some state in onCreate(), while we can optionally restore
// other state here, possibly usable after onStart() has completed.
// The savedInstanceState Bundle is same as the one used in onCreate().
    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        parqEscogido = (savedInstanceState.getString("EXTRA_NAME"));
        tiempo = (savedInstanceState.getString("EXTRA_TIME"));
    }

    // invoked when the activity may be temporarily destroyed, save the instance state here
    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putString("EXTRA_TIME", tiempo);
        outState.putString("EXTRA_NAME", parqEscogido);

        // call superclass to save any view hierarchy
        super.onSaveInstanceState(outState);
    }

    public boolean isNetworkReachable() {
        ConnectivityManager manager =
                (ConnectivityManager) getSystemService(this.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = manager.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected()) {
            return true;
        }
        return false;
    }
}
