package com.example.park21;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.lifecycle.ViewModelProviders;

import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.example.park21.models.Parqueadero;
import com.example.park21.viewmodels.PagarParqActivityViewModel;

import java.util.Calendar;

public class PagarParqActivity extends AppCompatActivity {

    private TextView numParq, ubicacionVehiculo, horaIngreso, descuento, valorPagra;
    private CardView registrarFactura, pagarPSE;
    private PagarParqActivityViewModel viewModel;
    private static String parqEscogido;
    private static String tiempo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pagar_parq);
        Intent intent = getIntent();
        viewModel = ViewModelProviders.of(this).get(PagarParqActivityViewModel.class);

        viewModel.init();

        parqEscogido = viewModel.getUbicacion();
        tiempo = viewModel.getTiempo();
        if (intent != null){
            Bundle extras = intent.getExtras();
            parqEscogido = viewModel.getUbicacion();
            tiempo = viewModel.getTiempo();
        }
        if(savedInstanceState != null){
            parqEscogido = viewModel.getUbicacion();
            tiempo = viewModel.getTiempo();
        }
        descuento = findViewById(R.id.descuento);
        descuento.setText("No hay descuento");
        registrarFactura = (CardView) findViewById(R.id.registrarFactura);
        registrarFactura.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent myIntent = new Intent(PagarParqActivity.this,
                        RegistrarFacturaActivity.class);
                startActivity(myIntent);
            }
        });

        viewModel = ViewModelProviders.of(this).get(PagarParqActivityViewModel.class);

        viewModel.init();
        valorPagra = findViewById(R.id.valorParqueadero);

        pagarPSE = (CardView) findViewById(R.id.pagarParqueadero);
        pagarPSE.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isNetworkReachable()){
                    Calendar c;
                    int mhour;
                    int mminute;
                    c = Calendar.getInstance();
                    mhour = c.get(Calendar.HOUR);
                    mminute = c.get(Calendar.MINUTE);
                    String tiempo2 = mhour+":"+mminute;
                    Toast.makeText(PagarParqActivity.this, "Hora de salida : "+tiempo2,
                            Toast.LENGTH_SHORT).show();
                    viewModel.pagar();
                    valorPagra.setText("1000");
                    pagarPSE.setEnabled(false);
                    registrarFactura.setEnabled(false);
                }
                else Toast.makeText(PagarParqActivity.this, "Debes estar conectado a internet para realizar esta operación.",
                        Toast.LENGTH_SHORT).show();
            }
        });

        numParq = findViewById(R.id.parqueaderoNumero);
        numParq.setText(parqEscogido);
        ubicacionVehiculo = findViewById(R.id.ubicacionVehiculo);
        ubicacionVehiculo.setText(parqEscogido);
        horaIngreso = findViewById(R.id.horaIngreso);
        horaIngreso.setText(tiempo);
        facturar();

    }

    public void facturar(){
        if (viewModel.getFactura()){
            descuento.setText("Hay descuento");
            registrarFactura.setEnabled(false);
        }
        if (viewModel.getPagar()){
            pagarPSE.setEnabled(false);
        }
    }

    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        parqEscogido = (savedInstanceState.getString("EXTRA_NAME"));
        tiempo = (savedInstanceState.getString("EXTRA_TIME"));
    }

    // invoked when the activity may be temporarily destroyed, save the instance state here
    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putString("EXTRA_TIME", tiempo);
        outState.putString("EXTRA_NAME", parqEscogido);

        // call superclass to save any view hierarchy
        super.onSaveInstanceState(outState);
    }

    public boolean isNetworkReachable() {
        ConnectivityManager manager =
                (ConnectivityManager) getSystemService(this.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = manager.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected()) {
            return true;
        }
        return false;
    }
}
