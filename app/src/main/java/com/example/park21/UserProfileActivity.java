package com.example.park21;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.example.park21.models.Familia;
import com.example.park21.models.Parqueadero;
import com.example.park21.models.Usuario;
import com.example.park21.repositories.Repository;
import com.example.park21.viewmodels.UserProfileActivityViewModel;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

import static androidx.constraintlayout.widget.Constraints.TAG;

public class UserProfileActivity extends AppCompatActivity {

    private FloatingActionButton mFab;
    private Button guardar;
    private UserProfileActivityViewModel userViewModel;
    private RecyclerViewAdapter mAdapter;
    private EditText nombre;
    private EditText telefono;
    private EditText edad;
    private EditText cedula;
    private EditText correo;
    private CircleImageView mImage;

    private Usuario usuario;
    private static Repository instance;
    private Familia familia;
    MutableLiveData<Usuario> data = new MutableLiveData<>();

    FirebaseFirestore db = FirebaseFirestore.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_profile);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mFab = findViewById(R.id.UserEdit);
        guardar = findViewById(R.id.guardarCambios);
        guardar.setEnabled(false);
        userViewModel = ViewModelProviders.of(this).get(UserProfileActivityViewModel.class);
        userViewModel.init();

        mImage = (CircleImageView) findViewById(R.id.image);
        nombre = (EditText) findViewById(R.id.editTextNombre);
        nombre.setEnabled(false);
        telefono = (EditText) findViewById(R.id.editTextTelefono);
        telefono.setEnabled(false);
        cedula = (EditText) findViewById(R.id.editTextCedula);
        cedula.setEnabled(false);
        correo = (EditText) findViewById(R.id.editTextCorreo);
        correo.setEnabled(false);
        edad = (EditText) findViewById(R.id.editTextEdad);
        edad.setEnabled(false);

        getData(new MyCallback() {
            @Override
            public void onCallback(Usuario eventList) {
                Log.d("TAG", "cosa");
            }
        });



        mFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isNetworkReachable()){
                    edad.setEnabled(true);
                    cedula.setEnabled(true);
                    telefono.setEnabled(true);
                    nombre.setEnabled(true);
                    guardar.setEnabled(true);
                }
                else Toast.makeText(UserProfileActivity.this, "Debes estar conectado a internet para realizar esta operación.",
                        Toast.LENGTH_SHORT).show();
            }
        });
        guardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (validate()){
                    if (isNetworkReachable()){
                        guardar.setEnabled(false);
                        edad.setEnabled(false);
                        correo.setEnabled(false);
                        cedula.setEnabled(false);
                        telefono.setEnabled(false);
                        nombre.setEnabled(false);
                        nombre = (EditText) findViewById(R.id.editTextNombre);
                        telefono = (EditText) findViewById(R.id.editTextTelefono);
                        cedula = (EditText) findViewById(R.id.editTextCedula);
                        edad = (EditText) findViewById(R.id.editTextEdad);

                        setData(new SetCallBack(){
                            @Override
                            public void onSetCallback(Usuario eventList){
                                Log.d("TAG", "cosa");
                            }
                        });
                    }
                    else Toast.makeText(UserProfileActivity.this, "Debes estar conectado a internet para realizar esta operación.",
                            Toast.LENGTH_SHORT).show();
                }
            }
        });



    }

    public boolean isNetworkReachable() {
        ConnectivityManager manager =
                (ConnectivityManager) getSystemService(this.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = manager.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected()) {
            return true;
        }
        return false;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    private void getData(MyCallback callback){
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        correo.setText(user.getEmail().toString());
        DocumentReference docRef = db.collection("Usuarios").document(
                user.getEmail().toString());
        docRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    DocumentSnapshot document = task.getResult();
                    if (document.exists()) {
                        Log.d(TAG, "DocumentSnapshot data: " + document.getData());
                        data.setValue(usuario);
                        edad.setText(document.get("edad").toString());
                        telefono.setText(document.get("telefono").toString());
                        nombre.setText(document.get("Nombre").toString());
                        cedula.setText(document.get("cedula").toString());
                        callback.onCallback(usuario);
                    } else {
                        Log.d(TAG, "No such document");
                    }
                } else {
                    Log.d(TAG, "get failed with ", task.getException());
                }
            }
        });
    }

    private void setData(SetCallBack callback){
        String nombreM = nombre.getText().toString();
        String telefonoM = telefono.getText().toString();
        String cedulaM = cedula.getText().toString();
        String edadM = edad.getText().toString();
        Map<String, Object> userio = new HashMap<>();
        userio.put("Nombre", nombreM);
        userio.put("telefono", telefonoM);
        userio.put("cedula", cedulaM);
        userio.put("edad", edadM);
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

        db.collection("Usuarios").document(user.getEmail().toString())
                .set(userio)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Log.d(TAG, "DocumentSnapshot successfully written!");
                        callback.onSetCallback(usuario);
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.w(TAG, "Error writing document", e);
                    }
                });
    }


    private interface MyCallback {
        void onCallback(Usuario eventList);
    }

    private interface SetCallBack {
        void onSetCallback(Usuario eventList);
    }

    public boolean validate() {
        boolean valid = true;

        String name = nombre.getText().toString();
        String address = cedula.getText().toString();
        String email = correo.getText().toString();
        String mobile = telefono.getText().toString();
        String Medad = edad.getText().toString();

        if (name.isEmpty() || name.length() < 3) {
            nombre.setError("Al menos 3 caracteres");
            valid = false;
        } else {
            nombre.setError(null);
        }

        if (address.isEmpty()) {
            cedula.setError("Ingresa una cédula válida");
            valid = false;
        } else {
            cedula.setError(null);
        }


        if (email.isEmpty() || !android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            correo.setError("Ingresa una dirección de email válida");
            valid = false;
        } else {
            correo.setError(null);
        }

        if (mobile.isEmpty() || mobile.length()!=10) {
            telefono.setError("Ingresa un número de télefono celular válido");
            valid = false;
        } else {
            telefono.setError(null);
        }

        if (Medad.isEmpty()) {
            edad.setError("Ingresa una edad válida");
            valid = false;
        } else {
            edad.setError(null);
        }

        return valid;
    }
}
