package com.example.park21.repositories;

import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;

import com.example.park21.models.Carro;
import com.example.park21.models.Familia;
import com.example.park21.models.Parqueadero;
import com.example.park21.models.Usuario;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firestore.v1.Document;

import java.util.ArrayList;
import java.util.List;

import static androidx.constraintlayout.widget.Constraints.TAG;

/**
 * Singleton pattern
 */
public class Repository {

    private static Repository instance;
    private ArrayList<Parqueadero> dataSetParqueaderos = new ArrayList<>();
    private Usuario usuario;
    private Familia familia;
    private boolean factura = false;
    private boolean pagar = false;
    private String tiempo;
    private String ubicación;
    MutableLiveData<Usuario> data = new MutableLiveData<>();

    FirebaseFirestore db = FirebaseFirestore.getInstance();

    public static Repository getInstance(){
        if(instance == null){
            instance = new Repository();
        }
        return instance;
    }


    // Get data from a webservice or online source
    public MutableLiveData<List<Parqueadero>> getNicePlaces(){
        Log.d("Hay datos?","A ver: "+dataSetParqueaderos.size());
        MutableLiveData<List<Parqueadero>> data = new MutableLiveData<>();
        data.setValue(dataSetParqueaderos);
        return data;
    }

    // Pretend to get data from a webservice or online source
    public MutableLiveData<List<Carro>> getCarros(){
        Carro carro1 = new Carro("abc 123");
        Carro carro2 = new Carro("trt 456");
        List<Carro> listaFamilia = new ArrayList<>();
        listaFamilia.add(carro1);
        listaFamilia.add(carro2);

        MutableLiveData<List<Carro>> data = new MutableLiveData<>();
        data.setValue(listaFamilia);
        return data;
    }

    // Get data from a webservice or online source
    public MutableLiveData<Usuario> getUsuario(){

        return data;
    }

    // Pretend to get data from a webservice or online source
    public MutableLiveData<Familia> getFamilia(){
        Usuario usuario1;
        Usuario usuario2;
        usuario1 = new Usuario("Martha Diaz","5454654654","25651156","soalal@pepo.com","54","Femenino", "https://i.redd.it/tpsnoz5bzo501.jpg");
        usuario2 = new Usuario("Mauro Diaz","54654651","2956515","smaurk@pepo.com","25","Masculino", "https://i.redd.it/j6myfqglup501.jpg");
        List<Usuario> listaFamilia = new ArrayList<>();
        listaFamilia.add(usuario1);
        listaFamilia.add(usuario2);
        listaFamilia.add(usuario);
        familia = new Familia(listaFamilia, "Padre");
        MutableLiveData<Familia> data = new MutableLiveData<>();
        data.setValue(familia);
        return data;
    }

    public void setNicePlaces(DocumentSnapshot document){

        Parqueadero parq = new Parqueadero(document.get("url").toString(),
                document.get("nombre").toString(),
                "",
                document.get("disponible").toString(),
                document.get("direccion").toString(),
                document.get("lat").toString(),
                document.get("lon").toString()
        );
//        boolean pop = false;
//        for(Parqueadero score : dataSetParqueaderos) {
//            if (score.getNombre()==(parq.getNombre())) {
//                pop = true;
//                Log.d("POP existe: ",parq.getNombre());
//            }
//        }
        if (dataSetParqueaderos.size()<3){
            dataSetParqueaderos.add(parq);
        }
    }

    public boolean facturas(){
        return factura;
    }

    public void setFacturas() {
        factura = true;
    }

    public boolean isPagar(){
        return pagar;
    }

    public void setPagar() {
        pagar = true;
    }

    public void setSalir(){
        factura=false;
        pagar = false;
        tiempo="";
        ubicación="";
    }

    public String getTiempo() {
        return tiempo;
    }

    public void setTiempo(String tiempo) {
        this.tiempo = tiempo;
    }

    public String getUbicación() {
        return ubicación;
    }

    public void setUbicación(String ubicación) {
        this.ubicación = ubicación;
    }
}
