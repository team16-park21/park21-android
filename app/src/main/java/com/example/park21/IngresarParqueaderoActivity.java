package com.example.park21;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProviders;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;

import com.example.park21.viewmodels.IngresarParqueaderoActivityViewModel;
import com.example.park21.viewmodels.MainActivityParqueaderoViewModel;
import com.example.park21.viewmodels.ParqueandoActivityViewModel;
import com.google.zxing.WriterException;

import java.util.Calendar;

import androidmads.library.qrgenearator.QRGContents;
import androidmads.library.qrgenearator.QRGEncoder;
import androidmads.library.qrgenearator.QRGSaver;

public class IngresarParqueaderoActivity extends AppCompatActivity {
    public static final String EXTRA_MESSAGE = "com.example.myfirstapp.MESSAGE";
    String TAG = "GenerateQRCode";
    ImageView qrImage;
    Button generar;
    Button irAParquear;
    Bitmap bitmap;
    QRGEncoder qrgEncoder;
    String inputValue;
    String parqEscogido;
    private IngresarParqueaderoActivityViewModel viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ingresar_parqueadero);
        // Get the Intent that started this activity and extract the string
        Intent intent = getIntent();
        parqEscogido = intent.getStringExtra(RecyclerViewAdapter.EXTRA_MESSAGE);
        Log.d(TAG, "Que hay "+parqEscogido);
        irAParquear = (Button) findViewById(R.id.buttonSeguiraParquear);
        irAParquear.setEnabled(false);
        viewModel = ViewModelProviders.of(this).get(IngresarParqueaderoActivityViewModel.class);

        viewModel.init();
        qrImage = (ImageView) findViewById(R.id.QR_Image);
        generar = (Button) findViewById(R.id.buttonGenerarQr);
        generar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                inputValue = "Prueba TODO";
                if (inputValue.length() > 0) {
                    WindowManager manager = (WindowManager) getSystemService(WINDOW_SERVICE);
                    Display display = manager.getDefaultDisplay();
                    Point point = new Point();
                    display.getSize(point);
                    int width = point.x;
                    int height = point.y;
                    int smallerDimension = width < height ? width : height;
                    smallerDimension = smallerDimension * 3 / 4;

                    qrgEncoder = new QRGEncoder(
                            inputValue, null,
                            QRGContents.Type.TEXT,
                            smallerDimension);
                    try {
                        bitmap = qrgEncoder.encodeAsBitmap();
                        qrImage.setImageBitmap(bitmap);
                    } catch (WriterException e) {
                        Log.v(TAG, e.toString());
                    }
                }
                generar.setEnabled(false);
                irAParquear.setEnabled(true);
            }
        });

        irAParquear.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {

                Intent myIntent = new Intent(IngresarParqueaderoActivity.this,
                        ParqueandoActivity.class);
                Calendar c;
                int mhour;
                int mminute;
                c = Calendar.getInstance();
                mhour = c.get(Calendar.HOUR);
                mminute = c.get(Calendar.MINUTE);
                String tiempo = mhour+":"+mminute;
                viewModel.setTiempo(tiempo);
                viewModel.setUbicacion(parqEscogido);
                Bundle extras = new Bundle();
                extras.putString("EXTRA_NAME",parqEscogido);
                extras.putString("EXTRA_TIME",tiempo);
                myIntent.putExtras(extras);
                startActivity(myIntent);
            }
        });

    }

    public boolean isNetworkReachable() {
        ConnectivityManager manager =
                (ConnectivityManager) getSystemService(this.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = manager.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected()) {
            return true;
        }
        return false;
    }


}
