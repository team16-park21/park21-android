package com.example.park21;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.example.park21.repositories.Repository;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.firestore.FirebaseFirestore;
import com.squareup.picasso.Picasso;

public class SnapshotAutomovilActivity extends AppCompatActivity {

    FloatingActionButton llamar;
    ImageView snapsho;
    private static Repository instance;
    FirebaseFirestore db = FirebaseFirestore.getInstance();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_snapshot_automovil);
        snapsho = findViewById(R.id.SnapShot);
        Picasso.get().load("https://imgcdn.larepublica.co/i/480/2017/11/17205958/Parqueaderos-ppal.jpg").into(snapsho);
        llamar = (FloatingActionButton) findViewById(R.id.buttonLlamarSeguridad);
        llamar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:3002776623"));
                startActivity(intent);
            }
        });
    }
}
