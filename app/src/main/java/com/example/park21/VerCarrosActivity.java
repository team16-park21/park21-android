package com.example.park21;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;

import com.example.park21.models.Carro;
import com.example.park21.viewmodels.CarrosActivityViewModel;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

public class VerCarrosActivity extends AppCompatActivity {

    private RecyclerView mRecyclerView;
    private CarrosRecyclerViewAdapter mAdapter;
    private CarrosActivityViewModel carrosActivityViewModel;
    private ProgressBar mProgressBar;
    private FloatingActionButton agregar;

    private Carro carro;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ver_carros);
        mRecyclerView = findViewById(R.id.recycler_view_carros);
        mProgressBar = findViewById(R.id.progress_bar_carro);
        agregar = findViewById(R.id.CarrosEdit);
        agregar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showAddItemDialog(VerCarrosActivity.this);
            }
        });
        carrosActivityViewModel = ViewModelProviders.of(this).get(CarrosActivityViewModel.class);

        carrosActivityViewModel.init();

        carrosActivityViewModel.getIsUpdating().observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(@Nullable Boolean aBoolean) {
                if(aBoolean){
                    showProgressBar();
                }
                else{
                    hideProgressBar();
                    mRecyclerView.smoothScrollToPosition(carrosActivityViewModel.getNicePlaces().getValue().size()-1);
                }
            }
        });

        initRecyclerView();
    }

    private void showProgressBar(){
        mProgressBar.setVisibility(View.VISIBLE);
    }

    private void hideProgressBar(){
        mProgressBar.setVisibility(View.GONE);
    }

    private void showAddItemDialog(Context c) {
        final EditText taskEditText = new EditText(c);
        AlertDialog dialog = new AlertDialog.Builder(c)
                .setTitle("Agrega un nuevo Carro")
                .setMessage("Agrega la placa de tu nuevo carro")
                .setView(taskEditText)
                .setPositiveButton("Agregar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String task = String.valueOf(taskEditText.getText());
                    }
                })
                .setNegativeButton("Cancelar", null)
                .create();
        dialog.show();
    }

    private void initRecyclerView(){

        mAdapter = new CarrosRecyclerViewAdapter(this, carrosActivityViewModel.getNicePlaces().getValue());
        RecyclerView.LayoutManager linearLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(linearLayoutManager);
        mRecyclerView.setAdapter(mAdapter);
    }
}
