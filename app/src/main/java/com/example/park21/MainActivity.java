package com.example.park21;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.Toast;

import com.example.park21.models.Parqueadero;
import com.example.park21.models.Usuario;
import com.example.park21.repositories.Repository;
import com.example.park21.viewmodels.MainActivityParqueaderoViewModel;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

import static androidx.constraintlayout.widget.Constraints.TAG;

public class MainActivity extends AppCompatActivity  implements OnMapReadyCallback{

    private static final String TAG = "MainActivity";

    // Try to obtain the map from the SupportMapFragment.
    private GoogleMap mMap;
    LocationManager locationManager;
    LocationListener locationListener;
    private FloatingActionButton mFab;
    private RecyclerView mRecyclerView;
    private RecyclerViewAdapter mAdapter;
    private ProgressBar mProgressBar;
    private static MainActivityParqueaderoViewModel mMainActivityViewModel;
    private static Repository instance;
    FirebaseFirestore db = FirebaseFirestore.getInstance();
    private MutableLiveData<List<Parqueadero>> mParqueaderos = new MutableLiveData<>();

    public static final String EXTRA_MESSAGE = "com.example.Park21.MESSAGE";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.AppTheme_Launcher);
        mMainActivityViewModel = ViewModelProviders.of(this).get(MainActivityParqueaderoViewModel.class);
        mMainActivityViewModel.init();
        LiveData<List<Parqueadero>> parqs = mMainActivityViewModel.getNicePlaces();
        if (parqs == null){
            getData(new MyCallback() {
                @Override
                public void onCallback(MutableLiveData<List<Parqueadero>> eventList) {

                }
            });
        }

        if (SaveSharedPreference.getUserName(MainActivity.this).length() == 0) {
            Intent intent = new Intent(this, LoginActivity.class);
            startActivity(intent);
        }
        setTheme(R.style.AppTheme);
        if (!isNetworkReachable()){
            Toast.makeText(MainActivity.this, "Debes estar conectado a internet para Ingresar a un parq",
                    Toast.LENGTH_SHORT).show();
        }

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync((OnMapReadyCallback) this);

        Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        mRecyclerView = findViewById(R.id.recyclerv_view);
        mProgressBar = findViewById(R.id.progress_bar);

    }


    public void todoBien(){
        Log.d("TAG", "tamaño: "+mMainActivityViewModel.getNicePlaces().getValue().size());
        initRecyclerView();
        mMainActivityViewModel.getNicePlaces().observe(MainActivity.this, new Observer<List<Parqueadero>>() {
            @Override
            public void onChanged(@Nullable List<Parqueadero> nicePlaces) {
                mAdapter.notifyDataSetChanged();
            }
        });

        mMainActivityViewModel.getIsUpdating().observe(MainActivity.this, new Observer<Boolean>() {
            @Override
            public void onChanged(@Nullable Boolean aBoolean) {
                if(aBoolean){
                    showProgressBar();
                }
                else{
                    hideProgressBar();
                    mRecyclerView.smoothScrollToPosition(mMainActivityViewModel.getNicePlaces().getValue().size()-1);
                }
            }
        });
        LiveData<List<Parqueadero>> parqs2 = mMainActivityViewModel.getNicePlaces();
        if (parqs2!=null){
            ArrayList<Parqueadero> mNicePlaces;
            mNicePlaces = new ArrayList<Parqueadero>();
            int numParq = mMainActivityViewModel.getNicePlaces().getValue().size();
            LatLng parq;
            for (int i = 0; i < numParq; i++){
                parq = new LatLng(Double.parseDouble(mMainActivityViewModel.getNicePlaces().getValue().get(i).getLat()), Double.parseDouble(mMainActivityViewModel.getNicePlaces().getValue().get(i).getLongitud()));
                mMap.addMarker(new MarkerOptions().position(parq).title("Marcador en "+mMainActivityViewModel.getNicePlaces().getValue().get(i).getTitle()));
            }
        }
    }

    private void getData(MyCallback callback){

        db.collection("parqueaderos")
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {

                            startHeavyProcessing(task);
                            callback.onCallback(mParqueaderos);

                        } else {
                            Log.d("Taggerman", "Error getting documents: ", task.getException());
                        }
                    }
                });
    }

    private interface MyCallback {
        void onCallback(MutableLiveData<List<Parqueadero>> eventList);
    }
    private void startHeavyProcessing(Task<QuerySnapshot> document){
        new LongOperation().execute(document);
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        Intent intent = getIntent();
        if (intent.getIntExtra("Place Number",0) == 0 ){

            // Zoom into users location
            locationManager = (LocationManager)this.getSystemService(Context.LOCATION_SERVICE);
            locationListener = new LocationListener() {
                @Override
                public void onLocationChanged(Location location) {
                    centreMapOnLocation(location,"Tu ubicación");
                }

                @Override
                public void onStatusChanged(String s, int i, Bundle bundle) {

                }

                @Override
                public void onProviderEnabled(String s) {

                }

                @Override
                public void onProviderDisabled(String s) {

                }
            };

            if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED){
                locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,0,0,locationListener);
                Location lastKnownLocation = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                centreMapOnLocation(lastKnownLocation,"Tu ubicación");
            } else {

                ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.ACCESS_FINE_LOCATION},1);
            }
        }


    }

    public void centreMapOnLocation(Location location, String title){

        LatLng userLocation = new LatLng(location.getLatitude(),location.getLongitude());
        //mMap.clear();
        mMap.addMarker(new MarkerOptions().position(userLocation).title(title));
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(userLocation,12));

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (grantResults.length>0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){

            if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED){
                locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,0,0,locationListener);

                Location lastKnownLocation = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                centreMapOnLocation(lastKnownLocation,"Tu ubicación");
            }
        }
    }

    private class LongOperation extends AsyncTask<Task<QuerySnapshot>, Void, String> {

        @Override
        protected String doInBackground(Task<QuerySnapshot>... task) {

            for (QueryDocumentSnapshot document : task[0].getResult()) {
                Log.d("Taggerman", document.getId() + " => " + document.getData());
                mMainActivityViewModel.addParq(document);
            }

            return "nada";
        }

        @Override
        protected void onPostExecute(String result) {
            mMainActivityViewModel.setNicePlacesStart();
            todoBien();
        }

        @Override
        protected void onPreExecute() {}

        @Override
        protected void onProgressUpdate(Void... values) {}
    }

    private void initRecyclerView(){

        mAdapter = new RecyclerViewAdapter(MainActivity.this, mMainActivityViewModel.getNicePlaces().getValue());
        RecyclerView.LayoutManager linearLayoutManager = new LinearLayoutManager(MainActivity.this);
        mRecyclerView.setLayoutManager(linearLayoutManager);
        mRecyclerView.setAdapter(mAdapter);
    }

    private void showProgressBar(){
        mProgressBar.setVisibility(View.VISIBLE);
    }

    private void hideProgressBar(){
        mProgressBar.setVisibility(View.GONE);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.top_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.action_profile:
                Intent a = new Intent(MainActivity.this,UserProfileActivity.class);
                startActivity(a);
                break;
            case R.id.action_logout:
                AlertDialog.Builder builder = new AlertDialog.Builder(this);

                builder.setTitle("Cerrar sesión");
                builder.setMessage("¿Quieres cerrar sesión?");

                builder.setPositiveButton("SI", new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {
                        FirebaseAuth.getInstance().signOut();
                        SaveSharedPreference.clearUserName(MainActivity.this);
                        Intent logout = new Intent(MainActivity.this,LoginActivity.class);
                        startActivity(logout);
                    }
                });

                builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        // Do nothing
                        dialog.dismiss();
                    }
                });

                AlertDialog alert = builder.create();
                alert.show();
                break;
            default:
                return super.onOptionsItemSelected(item);
        }
        return true;
    }

    @Override
    public void onDestroy(){
        locationManager.removeUpdates(locationListener);
        if (mMap != null) {
            mMap.setMyLocationEnabled(false);
        }
        super.onDestroy();

    }

    private boolean isNetworkReachable() {
        ConnectivityManager manager =
                (ConnectivityManager) getSystemService(this.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = manager.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected()) {
            return true;
        }
        return false;
    }
}
